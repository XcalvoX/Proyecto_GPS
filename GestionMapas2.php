<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GestionMapas2 extends Controller
{
     public function index()
    {
    	return \App\Mapa2::all();

    }

      public function shows($id)
    {
    	return \App\Mapa2::find($id);

    }


    public function store(Request $request,$id )
    {
    	return  \App\Mapa2::create($request->all());

    }

    public function edit(Request $request,$id)
    {

    	$registro=\App\Mapa2::finOrFail($id);
    	$registro-> update($request-> all());

    	return $registro;


    }

    public function destroy($id)
    {

    	$registro=\App\Mapa2::finOrFail($id);
    	$registro-> delete();


    	return 204;


    }
}
